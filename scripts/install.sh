#!/usr/bin/env bash

cd # Go back to the source file
cd Pyrolog/pyrolog_project
python setup.py build
python setup.py install
