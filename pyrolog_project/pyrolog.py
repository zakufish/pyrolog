#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__      = "Ricky Yu"
__copyright__   = "Copyright 2016, Ricky Yu"

import sys
import re

class pyrolog:
    database = dict()

    def __init__(self):
        self.database = self.__dict__

    def process_input(self, input_string):
        input_string = input_string.replace(" ", "") # Strip all spaces
        search_buffer = re.findall(".*\(", input_string)
        index = search_buffer[0].replace("(", "")

        logic_buffer = re.findall('\(.*\)', input_string)
        inner = logic_buffer[0].replace("(", "")
        inner = inner.replace(")", "")
        list_of_obj = re.split(",", inner)

        return_dict = {'index': index, 'list of obj': list_of_obj}
        return return_dict

    def compile(self, input_string):
        return_dict = self.process_input(input_string)

        if self.__dict__.has_key(return_dict['index']): # Key exists in database already
            if return_dict['list of obj'] in self.__dict__[return_dict['index']]:
                pass # Everything is in database already
            else:
                self.__dict__[return_dict['index']].append(return_dict['list of obj'])
        else:
            temp = list()
            temp.append(return_dict['list of obj'])
            self.__dict__[return_dict['index']] = temp

    def compare_compare(self, return_dict):
        index = return_dict['index']
        list_of_obj = return_dict['list of obj']

        list_of_keys = self.__dict__.keys()
        return_value = False
        for key in list_of_keys:
            if list_of_obj in list(self.__dict__[key]): # In order to prevent 'unhashed list' error
                return_value = True
                break
            else:
                pass

        print return_value
        return return_value

    def compare_find_val(self, return_dict):
        index = return_dict['index']
        list_of_obj = return_dict['list of obj']
        ref_list = list_of_obj
        var_element_list = list()
        var_element_index = list()

        for element in list_of_obj:
            if element[0].isupper():
                var_element_list.append(element) # Find the variable
                var_element_index.append(list_of_obj.index(element)) # Return the index
                ref_list.remove(element)

        value_dict = dict()

        for lists in list(self.__dict__[index]):
            temp_list = list()
            for element in lists: # Copy the list
                temp_list.append(element)
            for num in var_element_index:
                temp_list.remove(lists[num])
            if temp_list == ref_list:
                counter = 0
                for objects in var_element_list:
                    value_dict[objects] = lists[var_element_index[counter]]
                    counter += 1
                break
            else:
                pass

        print value_dict
        return value_dict

    def compare(self, input_string):
        return_dict = self.process_input(input_string)

        if self.__dict__.has_key(return_dict['index']):
            for element in return_dict['list of obj']:
                if element[0].isupper():
                    to_compare = True
                    break
                else:
                    to_compare = False
                    pass

            if to_compare == False:
                self.compare_compare(return_dict)
            else:
                self.compare_find_val(return_dict)
        else:
            print ">>> Invalid relationship - Index not found in database. Please recheck your input."
