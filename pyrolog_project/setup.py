#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__      = "Ricky Yu"
__copyright__   = "Copyright 2016, Ricky Yu"

from distutils.core import setup
setup(name='pyrolog',
      version='0.0.1',
      py_modules=['pyrolog'],
      )
