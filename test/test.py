"""
Initializing
"""
import sys
from StringIO import StringIO
from pyrolog import pyrolog

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        sys.stdout = self._stdout

"""
Simple test to check basic functions
"""
obj = pyrolog()
obj.compile('connected(a, b)')

with Capturing() as output:
    obj.compare('connected(a, b)')
    obj.compare('connected(a, c)')
    obj.compare('connected(b, a)')
    obj.compare('connected(c, a)')

answer = ['True', 'False', 'False', 'False']
assert output == answer

print 'test passed'
