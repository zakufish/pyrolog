# Pyrolog

Pyrolog is a logical programming library in Python that supports prolog-like functionalities.

  - Author: Ricky Yu
  - Dependecy: Python 2.7 (Haven't tested with Python3 --- I did the project in second semester of first year, didn't know Python3 at all).
  - Meanwhile, the Python 3 version will be released soon(Really?).

### Version
Demo version 0.0.1

### Tech
Pyrolog doesn't require any other open source project in order to work properly.

### Installation
```
cd Pyrolog/scripts
bash install.sh
```

### How To Use Pyrolog
First of all, you will need to create a Pyrolog object that is used to hold the construction of relationships:
```
from pyrolog import pyrolog
obj = pyrolog() # Create a pyrolog object
```
There are few functionalities you can use at the current stage.
```
>>> obj.compile('friend(joshua, alice)')
>>> obj.compare('friend(joshua, linda)')
>>> False
>>> obj.compare('friend(joshua, alice)')
>>> True
```
Also, you can find values by set the first character to capital to make it a variable:
```
>>> obj.compare('friend(joshua, Sam)')
>>> {'Sam' : 'alice'}
```

### Known Issue
Pyrolog doesn't support recursion --- which sucks because recursion is one of the most important features in prolog. I have no plan to implement that in the near future.